# trashfetch

little tui script written in python that shows some live stats and info about your computer

only works on unix because of psutil

for now it depends on https://pypi.org/project/py-cpuinfo/, i might read from /proc/cpuinfo sometime soon.

![](https://media.discordapp.net/attachments/249111029668249601/574253326561050625/unknown.png)

the program working, piped through https://github.com/jaseg/lolcat

![](https://gitlab.com/raii/trashfetch/raw/master/out.gif)